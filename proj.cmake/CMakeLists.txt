cmake_minimum_required (VERSION 2.6)
project (video-test)

add_subdirectory(../../oxygine-framework oxygine-framework)
add_definitions(${OXYGINE_DEFINITIONS})
include_directories(${OXYGINE_INCLUDE_DIRS})
link_directories(${OXYGINE_LIBRARY_DIRS})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OXYGINE_CXX_FLAGS} -std=c++11")

set(SOURCES
    ../src/main.cpp
    ../src/app.h
    ../src/app.cpp
    ../src/ThreadWorker.h
    ../src/ThreadWorker.cpp
    ../src/VideoPlayer.h
    ../src/VideoPlayer.cpp
    )

add_executable(video-test ${SOURCES})
target_link_libraries(video-test ${OXYGINE_CORE_LIBS} avformat avcodec swscale avutil z)
