#include "VideoPlayer.h"
#include <chrono>
#include "core/gl/oxgl.h"

using namespace std::chrono;

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

#include "ThreadWorker.h"

struct AVData
{
    AVFormatContext     *pFormatCtx = nullptr;
    int                 videoStream = -1;
    AVCodecContext      *pCodecCtxOrig = nullptr;
    AVCodecContext      *pCodecCtx = nullptr;
    AVCodec             *pCodec = nullptr;
    AVFrame             *pFrame = nullptr;
    AVFrame             *pFrameRGB = nullptr;
    uint8_t             *buffer = nullptr;
    SwsContext          *sws_ctx = nullptr;

    std::atomic<timeMS> best_effort_timestamp{0}; //time for frame
};

static void updateTextureData(spNativeTexture texture, AVFrame* frame)
{
    //auto startTime = high_resolution_clock::now();

    nativeTextureHandle texId = texture->getHandle();
    int width = texture->getWidth();
    int height = texture->getHeight();

    glBindTexture(GL_TEXTURE_2D, (GLuint)((intptr_t) texId)); //hack cast

    //HACK: redefine pixel format, but NativeTexture object doesn't know about it
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width,
                    height, GL_RGB, GL_UNSIGNED_BYTE, frame->data[0]);

    //auto endTime = high_resolution_clock::now();
    //long long textureTime = duration_cast<milliseconds>(endTime - startTime).count();
    //log::messageln("texture copy time: %ld", textureTime);
}

static void updateTextureData(MemoryTexture* texture, AVFrame* frame)
{
    //auto startTime = high_resolution_clock::now();

    ImageData lockedData = texture->lock(Texture::lock_write, nullptr);
    int width = texture->getWidth();
    int height = texture->getHeight();
    //TODO: more efficient
    unsigned char* ptr = lockedData.data;
    for (int y = 0; y < height; ++y)
    {
        uint8_t* srcData = frame->data[0] + y * frame->linesize[0];
        for (int x = 0; x < width; ++x)
        {
            //log::messageln("x: %d, y: %d", x, y);
            //ptr[0] = 255;
            //ptr[1] = 155;
            //ptr[2] = 200;
            ptr[0] = srcData[0]; //r
            ptr[1] = srcData[1]; //g
            ptr[2] = srcData[2]; //b
            ptr[3] = 255; //a

            ptr += 4;
            srcData += 3;
        }
    }
    //really no unlock is needed for MemoryTexture

    //auto endTime = high_resolution_clock::now();
    //long long textureTime = duration_cast<milliseconds>(endTime - startTime).count();
    //log::messageln("texture copy time: %ld", textureTime);
}

VideoPlayer::VideoPlayer(const std::string& filename, bool useMemoryTexture) :
    _useMt(useMemoryTexture),
    _dirty(false),
    _videoTime(0),
    _data(nullptr),
    _worker(new ThreadWorker())
{
    setVideo(filename);
    nextFrame();
}

VideoPlayer::~VideoPlayer()
{
    freeAvData();
}

void VideoPlayer::update(timeMS dt)
{
    _videoTime += dt;
    if (_data->best_effort_timestamp <= _videoTime)
        nextFrameAsync();
    updateTexture();
}

AnimationFrame VideoPlayer::getAnimFrame() const
{
    AnimationFrame frame;
    Diffuse df;
    df.base = _texture;
    int width = _texture->getWidth();
    int height = _texture->getHeight();
    frame.init(0, df,
               RectF(0, 0, 1, 1),
               RectF(0, 0, width, height),
               Vector2(width, height));
    return frame;
}

void VideoPlayer::setVideo(const std::string& filename)
{
    std::lock_guard<std::mutex> g(_mutex);

    if (_data)
        freeAvData(); //clear previous data;
    _data = new AVData();

    // Open video file
    if (avformat_open_input(&_data->pFormatCtx, filename.c_str(), NULL, NULL) != 0)
        return; // Couldn't open file

    // Retrieve stream information
    if (avformat_find_stream_info(_data->pFormatCtx, NULL) < 0)
        return; // Couldn't find stream information

    // Dump information about file onto standard error
    av_dump_format(_data->pFormatCtx, 0, filename.c_str(), 0);

    // Find the first video stream
    _data->videoStream = -1;
    for (int i = 0; i < (int)_data->pFormatCtx->nb_streams; i++)
    {
        if (_data->pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            _data->videoStream = i;
            break;
        }
    }
    if (_data->videoStream == -1)
        return; // Didn't find a video stream

    // Get a pointer to the codec context for the video stream
    _data->pCodecCtxOrig = _data->pFormatCtx->streams[_data->videoStream]->codec;
    // Find the decoder for the video stream
    _data->pCodec = avcodec_find_decoder(_data->pCodecCtxOrig->codec_id);
    if (_data->pCodec == NULL)
    {
        fprintf(stderr, "Unsupported codec!\n");
        return; // Codec not found
    }

    // Copy context
    _data->pCodecCtx = avcodec_alloc_context3(_data->pCodec);
    if (avcodec_copy_context(_data->pCodecCtx, _data->pCodecCtxOrig) != 0)
    {
        fprintf(stderr, "Couldn't copy codec context");
        return; // Error copying codec context
    }

    // Open codec
    if (avcodec_open2(_data->pCodecCtx, _data->pCodec, NULL) < 0)
        return; // Could not open codec

    // Allocate video frame
    _data->pFrame=av_frame_alloc();

    // Allocate an AVFrame structure
    _data->pFrameRGB = av_frame_alloc();
    if (_data->pFrameRGB == NULL)
        return;

    initTexture(_data->pCodecCtx->width, _data->pCodecCtx->height);

    // Determine required buffer size and allocate buffer
    {
        int numBytes = avpicture_get_size(PIX_FMT_RGB24, _data->pCodecCtx->width, _data->pCodecCtx->height);
        _data->buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));
    }

    // Assign appropriate parts of buffer to image planes in pFrameRGB
    // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
    // of AVPicture
    avpicture_fill((AVPicture *)_data->pFrameRGB, _data->buffer, PIX_FMT_RGB24,
            _data->pCodecCtx->width, _data->pCodecCtx->height);

    // initialize SWS context for software scaling
    _data->sws_ctx = sws_getContext(_data->pCodecCtx->width,
            _data->pCodecCtx->height,
            _data->pCodecCtx->pix_fmt,
            _data->pCodecCtx->width,
            _data->pCodecCtx->height,
            PIX_FMT_RGB24,
            SWS_BILINEAR,
            NULL,
            NULL,
            NULL
                                    );
}

void VideoPlayer::initAV()
{
    av_register_all();
}

void VideoPlayer::nextFrame()
{
    std::lock_guard<std::mutex> g(_mutex);
    if (_data == nullptr)
        return;

    //auto startTime = high_resolution_clock::now();
    //log::messageln("trying to get next video frame");
    AVPacket packet;
    int frameFinished = 0;
    while (frameFinished == 0)
    {
        if (av_read_frame(_data->pFormatCtx, &packet) >= 0)
        {
            // Is this a packet from the video stream?
            if (packet.stream_index == _data->videoStream)
            {
                // Decode video frame
                avcodec_decode_video2(_data->pCodecCtx, _data->pFrame, &frameFinished, &packet);

                // Did we get a video frame?
                if (frameFinished)
                {
                    // Convert the image from its native format to RGB
                    sws_scale(_data->sws_ctx, (uint8_t const * const *)_data->pFrame->data,
                            _data->pFrame->linesize, 0, _data->pCodecCtx->height,
                            _data->pFrameRGB->data, _data->pFrameRGB->linesize);

                    const double frameDelay = av_q2d(_data->pFormatCtx->streams[_data->videoStream]->time_base);
                    const timeMS pts = packet.pts * frameDelay * 1000;
                    _data->best_effort_timestamp = pts;

                    //auto frameFinishedTime = high_resolution_clock::now();
                    //long long avcodecTime = duration_cast<milliseconds>(frameFinishedTime - startTime).count();
                    //log::messageln("time elapsed for avcodec: %ld", avcodecTime);

                    //log::messageln("frameDelay: %f, pts: %ld", frameDelay, pts);

                    _dirty = true;

                    if (_useMt)
                        updateTextureData(&_mt, _data->pFrameRGB);
                }
            }

            // Free the packet that was allocated by av_read_frame
            av_free_packet(&packet);
        }
        else
        {
            av_seek_frame(_data->pFormatCtx, _data->videoStream,
                    0, AVSEEK_FLAG_BACKWARD);
            _videoTime = 0;
            _data->best_effort_timestamp = 0;
            frameFinished = true;
        }
    }
}

void VideoPlayer::nextFrameAsync()
{
    _worker->appendFn([this]()
    {
        nextFrame();
    });
}

void VideoPlayer::freeAvData()
{
    std::lock_guard<std::mutex> g(_mutex);
    // Free the RGB image
    av_free(_data->buffer);
    av_frame_free(&_data->pFrameRGB);

    // Free the YUV frame
    av_frame_free(&_data->pFrame);

    // Close the codecs
    avcodec_close(_data->pCodecCtx);
    avcodec_close(_data->pCodecCtxOrig);

    // Close the video file
    avformat_close_input(&_data->pFormatCtx);

    delete _data;
}

void VideoPlayer::initTexture(int width, int height)
{
    if (_useMt)
        _mt.init(width, height, TF_R8G8B8A8);

    _texture = IVideoDriver::instance->createTexture();
    //NOTE: cannot define RGB format because oxygine doesn't support it
    _texture->init(width, height, TF_R8G8B8A8, false);

    //log::messageln("initialized texture w: %d, h: %d", width, height);

}

void VideoPlayer::updateTexture()
{
    //copy image to texture
    if (_dirty)
    {
        std::lock_guard<std::mutex> g(_mutex);
        if (_useMt)
        {
            _texture->updateRegion(0, 0, _mt.lock());
        }
        else
        {
            updateTextureData(_texture, _data->pFrameRGB);
        }
        _dirty = false;
    }
}
