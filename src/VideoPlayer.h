#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

struct AVData;
class ThreadWorker;

#include "oxygine-framework.h"
#include <memory>
#include <mutex>
#include <atomic>

using namespace oxygine;

DECLARE_SMART(VideoPlayer, spVideoPlayer)
class VideoPlayer : public ref_counter
{
public:
    VideoPlayer(const std::string& filename, bool useMemoryTexture = false);
    virtual ~VideoPlayer();

    static void initAV();
    void update(timeMS dt);

    AnimationFrame getAnimFrame() const;
private:
    const bool _useMt;
    spNativeTexture _texture;
    MemoryTexture _mt;
    std::atomic<bool> _dirty; //texture update is needed
    timeMS _videoTime;
    AVData* _data; //struct to incapsulate interaction with avconv
    std::unique_ptr<ThreadWorker> _worker;
    std::mutex _mutex;

    void setVideo(const std::string& filename);
    void nextFrame();
    void nextFrameAsync();
    void freeAvData();
    void initTexture(int width, int height);
    void updateTexture();
};

#endif // VIDEOPLAYER_H
