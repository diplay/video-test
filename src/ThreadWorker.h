#ifndef THREADWORKER_H
#define THREADWORKER_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <functional>

typedef std::function<void()> fn_type;

//worker implementation from
//habrahabr.ru/post/188234/
class ThreadWorker
{
public:

    ThreadWorker();
    ~ThreadWorker();

    void appendFn(fn_type fn);
    size_t getTaskCount();
    bool isEmpty();

private:
    bool enabled;
    std::condition_variable	cv;
    std::queue<fn_type>	fqueue;
    std::mutex mutex;
    std::thread	thread;

    void thread_fn();
};

#endif // THREADWORKER_H
