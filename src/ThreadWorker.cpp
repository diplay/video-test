#include "ThreadWorker.h"

ThreadWorker::ThreadWorker()
    :enabled(true),fqueue()
    ,thread(&ThreadWorker::thread_fn, this)
{}

ThreadWorker::~ThreadWorker()
{
    enabled = false;
    cv.notify_one();
    thread.join();
}

void ThreadWorker::appendFn(fn_type fn)
{
    std::unique_lock<std::mutex> locker(mutex);
    fqueue.push(fn);
    cv.notify_one();
}

size_t ThreadWorker::getTaskCount()
{
    std::unique_lock<std::mutex> locker(mutex);
    return fqueue.size();
}

bool ThreadWorker::isEmpty()
{
    std::unique_lock<std::mutex> locker(mutex);
    return fqueue.empty();
}

void ThreadWorker::thread_fn()
{
    while (enabled)
    {
        std::unique_lock<std::mutex> locker(mutex);
        cv.wait(locker, [&](){ return !fqueue.empty() || !enabled; });
        while(!fqueue.empty())
        {
            fn_type fn = fqueue.front();
            locker.unlock();
            fn();
            locker.lock();
            fqueue.pop();
        }
    }
}
