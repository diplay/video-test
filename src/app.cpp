#include "VideoPlayer.h"

void app_preinit(int, char**)
{
}

void app_init()
{
    VideoPlayer::initAV();

    //spVideoPlayer player1 = new VideoPlayer("data/test.mp4", false);
    spVideoPlayer player2 = new VideoPlayer("data/test.mkv", true);

    getStage()->setCallbackDoUpdate([=](const UpdateState& us)
    {
        //player1->update(us.dt);
        player2->update(us.dt);
    });

    spSprite a = new Sprite();
    a->setAnimFrame(player2->getAnimFrame());
    a->attachTo(getStage());

    spSprite b = new Sprite();
    b->setAnimFrame(player2->getAnimFrame());
    b->setY(550);
    b->attachTo(getStage());
}

void app_update()
{
}

void app_destroy()
{
}
